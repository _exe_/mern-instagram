import React, {useState, useEffect} from 'react';
import {Redirect, Route, Switch} from 'react-router'
import { useSelector } from 'react-redux'
import HomePage from "../page/HomePage/HomePage";
import ProtectedRoute from './ProtectedRoute';
import LoginPage from '../page/Landing/LoginPage';
import RegisterPage from '../page/Landing/RegisterPage';
import User from "../page/User";

const AppRouters = () => {
    const isAuth = useSelector(state => state.user.isAuth);
    const [isAuthenticated, setIsAuthenticated] = useState(isAuth)

    useEffect(() => {
      const token = JSON.parse(localStorage.getItem('token'))
      if(token && isAuth){
        setIsAuthenticated(true)
      } else {
        setIsAuthenticated(false)
      }
    }, [isAuth])

    useEffect(()=> {
      const token = JSON.parse(localStorage.getItem('token'))
      if(token && isAuth){
        setIsAuthenticated(true)
      } else {
        setIsAuthenticated(false)
      }
    }, [])
   
    
    return (
              <Switch>
                <Route exact path="/login" isAuth={isAuthenticated}  render={(restProps)=> {
                    if (isAuth) {
                      return <Redirect to='/'/>
                    }
                    return <LoginPage isAuth={isAuthenticated} {...restProps}/>
                  }       
                } />
                <Route exact path="/register" isAuth={isAuthenticated}  render={(restProps)=> {
                    if (isAuth) {
                      return <Redirect to='/'/>
                    }
                    return <RegisterPage isAuth={isAuthenticated} {...restProps}/>
                  }       
                } />
                <ProtectedRoute exact path='/' isAuth={isAuthenticated} component={HomePage}/>
                <ProtectedRoute exact path='/user/:id' isAuth={isAuthenticated} component={User}/>
                <ProtectedRoute exect path='/' isAuth={isAuthenticated} component={HomePage}/>
                {/* <Route path="*" component={Page404}/> */}               
            </Switch>
    );
};

export default AppRouters;