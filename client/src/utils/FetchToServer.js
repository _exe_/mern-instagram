import postActions from "../redux/Actions/postActions";

const url = "/api/v1";


export const getAllPosts = (currentPage) => {
    return fetch(`${url}/posts/`, {
        headers: {
            Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
        }
    })
        .then(res => {
            return res.json();
        })
        .then(data => {
            return data.data.filter((el, index) => index < currentPage * 3);
        });
};

export const likePost = (like, data) => {
    return fetch(`${url}/posts/${like ? "unlike" : "like"}`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
        },
        body: JSON.stringify(data),
    });
};

export const updatePost = (id, data) => {
    return fetch(`${url}/posts/:${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json());

};

export const createPostPut = (data) => {
    return fetch(`${url}/posts/craete`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json()).then(data => {
            return data.data;
        });
};



export const loginUser = async (credentials)=> {
    return fetch(`${url}/auth/login`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(credentials)
    })
    .then(res=> res.json());
}

export const registerUser = async (credentials)=> {
    return fetch(`${url}/auth/register`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(credentials)
    })
    .then(res=> res.json());
}

export const getSuggestions = async ()=> {
    return fetch(`${url}/user/suggestions`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+ JSON.parse(localStorage.getItem('token'))
        },
    })
    .then(res=> res.json())
}

export const deletePostFetch = (id) => {
    return fetch(`${url}/posts/:${id}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
        },
    }).then(res => res.json()).then(console.log);
};

export const commentPostFetch = (data) => {
    return fetch(`${url}/posts/comment`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
        },
        body: JSON.stringify(data),
    }).then(console.log);
};


export const patchFollow = (data)=> {
    return fetch(`${url}/user/follow`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+ JSON.parse(localStorage.getItem('token'))
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json())
}

export const patchUnFollow = (data)=> {
    return fetch(`${url}/user/unfollow`, {})

};
export const getAllUserPost = async (id) => {
    try {
        const data = await fetch(`${url}/user/${id}`, {
            headers: {
                Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
            }
        });
        return await data.json();
    } catch (e) {
        console.error(e);
    }
}
export const getSinglePost = async (id) => {
    try {
        const data = await fetch(`${url}/posts/single/${id}`, {
            headers: {
                Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
            }
        });
        return await data.json();
    } catch (e) {
        console.error(e);
    }
};

export const followUser = (follow, followId) => {
    try {
        return fetch(`${url}/user/${follow ? "follow" : "unfollow"}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + JSON.parse(localStorage.getItem('token'))
            },
            body: JSON.stringify(followId),
        }).then(console.log(`${url}/user/${follow ? "follow" : "unfollow"}`));

    } catch (e) {
        console.error(e);
    }

};
