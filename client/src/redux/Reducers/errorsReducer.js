import {USER_LOGIN_FAIL,SUBSCRIBE_TO_USER_FAIL,UNSUBSCRIBE_FROM_USER_FAIL,ADD_LIKE_TO_POST_FAIL,REMOVE_LIKE_FROM_POST_FAIL,SAVE_POSTS_FAIL,CREATE_POST_FAIL,SET_SUGGESTION_FAIL,REGISTER_FAIL,USER_FOLLOW_FAIL,CLEAR_ERROR_STATE } from "../ActionTypes/errorActionTypes"
const initialState = "";

const errorsReducer = (state = initialState, action) => {
  switch (action.type) {
      case USER_LOGIN_FAIL:
       return  action.payload
      case SUBSCRIBE_TO_USER_FAIL:
       return  action.payload
      case UNSUBSCRIBE_FROM_USER_FAIL:
         return  action.payload
      case ADD_LIKE_TO_POST_FAIL:
          return  action.payload
      case REMOVE_LIKE_FROM_POST_FAIL:
         return  action.payload
      case SAVE_POSTS_FAIL:
         return  action.payload
      case CREATE_POST_FAIL:
         return  action.payload
      case SET_SUGGESTION_FAIL:
         return  action.payload      
      case REGISTER_FAIL:
         return  action.payload
      case USER_FOLLOW_FAIL:
         return  action.payload
      case CLEAR_ERROR_STATE:
         return  ''
      default:
          return state;
  }
};

export default errorsReducer;