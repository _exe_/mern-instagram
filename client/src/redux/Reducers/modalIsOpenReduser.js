import {MODAL_TOGGLE} from "../ActionTypes/userActionTypes";

const initialState = false;

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case MODAL_TOGGLE:
            return !state;
        default:
            return state;
    }
};

export default modalReducer;