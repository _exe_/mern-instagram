import {combineReducers} from "redux";
import userReducer from "./userReducer";
import postReducer from "./postReducer";
import modalReducer from "./modalIsOpenReduser";
import errorsReducer from './errorsReducer'

const rootReducer = combineReducers({
    user: userReducer,
    post: postReducer,
    modal: modalReducer,
    errors: errorsReducer
});

// const rootReducer = (state = {user: [], post: []}, action) => {
//     switch (action.type) {
//         default:
//             return state;
//     }
// }

export default rootReducer;