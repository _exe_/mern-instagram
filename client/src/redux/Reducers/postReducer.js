import {SAVE_POSTS, CREATE_POST} from "../ActionTypes/userActionTypes";

const initialState = [];

const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_POSTS:
            return [...action.payload];
        case CREATE_POST:
            return [action.payload, ...state];
        default:
            return state;
    }
};

export default postReducer;