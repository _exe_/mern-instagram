import {USER_LOGIN, SET_SUGGESTION, UNSUBSCRIBE_FROM_USER, SUBSCRIBE_TO_USER, REGISTER_SUCCESSFULLY, REGISTER_FAIL, USER_LOGIN_FAIL } from '../ActionTypes/userActionTypes'
import {getSuggestions, loginUser, registerUser, patchFollow, patchUnFollow} from "../../utils/FetchToServer";
import errorsAction from './errorsAction';

const loginSuccess = (data)=>({
  type: USER_LOGIN,
  payload: data
});


const suggestion = (data)=> ({
  type: SET_SUGGESTION,
  payload: data
})

const follow = ()=> ({
  type: SUBSCRIBE_TO_USER
})
const unfollow = ()=> ({
  type: UNSUBSCRIBE_FROM_USER
})

const registerSuccess = ()=> ({
  type: REGISTER_SUCCESSFULLY
})





const loginAsync = (creadentials) => async dispatch => {
    const res = await loginUser(creadentials);
    if (res.success) {
      const token = res.data.token;
      localStorage.setItem('token', JSON.stringify(token))
      dispatch(loginSuccess(res.data.user))
    } else{
      dispatch(errorsAction.loginError(res.error))
    }
};

const registerAsync = (creadentials) => async dispatch=> {
  const res = await registerUser(creadentials);
  if (res.success) {
    dispatch(registerSuccess())  
  }
  else{
    dispatch(errorsAction.registerError(res.error))
  }
};
const getSuggestionsAsync = ()=> async dispatch=> {
  const res = await getSuggestions()
  if (res.success) {
    const data = res.data;
    dispatch(suggestion(data))
  } else{
    dispatch(errorsAction.suggestionError(res.error))
  }
}

const followUserAsync = (id)=> async dispatch=> {
  const obj = {followId: id}
  const res = await patchFollow(obj)
  if (res.success) {
    const data = res.data;
    dispatch(follow(data))
  } else{
    dispatch(errorsAction.followError(res.error))
  }
}

const unFollowUserAsync = (id)=> async dispatch=> {
  const obj = {followId: id}
  const res = await patchUnFollow(obj)
  if (res.success) {
    const data = res.data;
    dispatch(unfollow(data))
  } else{
    dispatch(errorsAction.unFollowError(res.error))
  }
}


export default {
  loginAsync,
  registerAsync,
  getSuggestionsAsync,
  followUserAsync,
  unFollowUserAsync
}