import {useEffect, useState} from "react";
import Header from "../components/userComponents/Header/Header";
import Posts from "../components/userComponents/Post/Posts";
import PostDetails from "../components/PostDetails/PostDetails";
import Footer from "../components/userComponents/Footer/Footer";
import "./User.scss";

import {getAllUserPost, getSinglePost} from "../utils/FetchToServer";

const User = ({match}) => {
    const [user, setUser] = useState([]);
    const [posts, setPosts] = useState([]);
    const [currentPost, setCurrentPost] = useState(null);
    const [currentPostId, setCurrentPostId] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const [isUpdated, setIsUpdated] = useState(true);


    useEffect(() => {
        setIsLoading(true);
        getAllUserPost(match.params.id).then(res => setUser(res.data));
        setIsLoading(false);
        setIsUpdated(true);
    }, [isUpdated]);

    useEffect(() => {
        setIsLoading(true);
        getAllUserPost(match.params.id).then(res => setPosts(res.data));
        setIsLoading(false);
    }, [currentPost]);

    useEffect(() => {
        if (currentPostId !== null) {
            setIsLoading(true);
            getSinglePost(currentPostId)
                .then(res => setCurrentPost(res.data));
            setIsLoading(false);
            setIsUpdated(true);
        }
    }, [currentPostId, isUpdated]);

    const showPost = (postId) => {
        setCurrentPostId(postId);
        setShowModal(true);
        document.body.classList.add("modal_open");
    };

    const closeModal = () => {
        setShowModal(false);
        setCurrentPostId(null);
        setCurrentPost(null);
        document.body.classList.remove("modal_open");
    };

    const updateInfo = (flag) => {
        setIsUpdated(flag);
    };

    return (
        <div className="container">
            {user.length !== 0 && posts.length !== 0 &&
            <Header
                user={user.user}
                postCount={posts.posts.length}
                updateInfo={updateInfo}/>}
            {isLoading && (<div>Loading...</div>)}
            {user.length !== 0 && posts.length !== 0 &&
            <Posts posts={posts.posts} userAvatar={user.user.photo} showPost={showPost}/>}
            {showModal && currentPost &&
            <PostDetails currentPost={currentPost} closeModal={closeModal} updateInfo={updateInfo}/>}
            <Footer/>
        </div>
    );
};

export default User;