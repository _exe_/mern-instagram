import React from 'react';
import './Header.scss';
import Icons from '../../svgIcons/Icons'
import {useDispatch, useSelector} from "react-redux";
import modalAction from "../../redux/Actions/modalAction";

const Header = ({handleClickOnCreatePostTest}) => {
    const modalIsOpen = useSelector(state => state.modal);
    const dispatch = useDispatch();

    const handleClickOnCreatePost = ()=> {
        dispatch(modalAction.toggleModal(!modalIsOpen))
    };

    return (
        <header className='header'>
            <h1 className='header__logo'><Icons type='logoInstagram'/></h1>
           <button data-testid='bth-create-post' className='header__bth-createPost' onClick={handleClickOnCreatePostTest || handleClickOnCreatePost}></button>
        </header>
    );
};

export default Header;