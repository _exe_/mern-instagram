import PostItem from "./PostItem";
import "./Posts.scss";

const Posts = ({posts, showPost}) => {
        const post = posts.map(item => (
            <div key={item.id} onClick={() => showPost(item.id)}>
                <PostItem
                    photo={item.photo}
                    comments={item.comments}
                    likes={item.likes}
                />
            </div>
        )).reverse();
        return (
            <div className="user_posts">
                {post}
            </div>
        );
    }
;

export default Posts;