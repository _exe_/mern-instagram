import "./Comment.scss";

const Comment = ({comments}) => {
    const {text, postedBy: {name, photo},} = comments;
    return (
        <div className="comment">
            <div className="comment__user_info">
                <div className="comment__img_container">
                    <img src={photo} alt="" className="comment__img"/>
                </div>
                <div className="comment__author">{name}</div>
            </div>
            <div className="comment__body">{text}</div>
        </div>
    );
};

export default Comment;