import React, {useState} from 'react';
import {Formik, Form, Field, ErrorMessage} from "formik";
import postActions from "../../redux/Actions/postActions";
import {useDispatch, useSelector} from "react-redux";
import './Modal.scss'
import modalAction from "../../redux/Actions/modalAction";

const Modal = () => {
    const dispatch = useDispatch();
    const modalIsOpen = useSelector(state => state.modal);
    const [selectedFile, setSelectedFile] = useState('');

    const handleFileInputChange = (e) => {
        setSelectedFile(e.target.files[0]);
    };

    const handleSubmit = async (values) => {
        if (!selectedFile) return;
        const reader = new FileReader();
        reader.readAsDataURL(selectedFile);
        reader.onloadend = () => {
            dispatch(postActions.createPostAsync({...values, photo: reader.result}));
            dispatch(modalAction.toggleModal(!modalIsOpen));
        };
    };

    const handleClickForCloseModal = (e) => {
        if (e.target.className !== 'create-post__wrapper-container') return;
        dispatch(modalAction.toggleModal(!modalIsOpen));
    };

    return (
        <div onClick={e => handleClickForCloseModal(e)} className='create-post__wrapper-container'>
            <Formik
                initialValues={{
                    photo: null,
                    body: '',
                    title: '',
                }}

                onSubmit={handleSubmit}
            >
                {() => {
                    return <Form className='create-post__form' noValidate action="">
                        <Field className='create-post__input' as='textarea' type='text' name='title' placeholder='enter title'/>
                        <Field className='create-post__input' as='textarea' type='text' name='body' placeholder='enter body'/>
                        <label htmlFor="file" className='create-post__file-label'>Choose a photo</label>
                        <input onChange={handleFileInputChange}  className='create-post__file' id="file" name="photo" type="file"/>
                        <button className='create-post__bth-send' type='submit'>publish</button>
                    </Form>
                }

                }
            </Formik>
        </div>
    );
};

export default Modal;