import React from 'react';
import PropTypes from 'prop-types';
import UserItem from './UserItem';
import './UserList.scss'

const UserList = props => {
  const {users, canSub, barTitle} = props;

  const renderList = function(){
    if (canSub) {
      return users.map(user => {
        return(<UserItem key={user._id} user={user} canSub={true} />)
       })
    } else{
      return users.map(user=> {
        return (<UserItem key={user._id} user={user}/>)
      })
    }
  }
 
  return (
    <div className="user">
      <p className="user__title">
          {barTitle}
      </p>
      <div className="user__list">
      {renderList()}
      </div>
    </div>
  );
};

UserList.propTypes = {
  users: PropTypes.array.isRequired,
  canSub: PropTypes.bool.isRequired,
  barTitle: PropTypes.string.isRequired,

};

export default UserList;