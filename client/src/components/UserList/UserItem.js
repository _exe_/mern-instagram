import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import userActions from '../../redux/Actions/userActions';



const UserItem = ({user = {}, canSub = false}) => {
  const [isSubscribe, setisSubscribe] = useState(false)
  const dispatch = useDispatch();
  const handleSubscribe = (userId)=> {
    if (!isSubscribe) {
      dispatch(userActions.followUserAsync(userId))
      setisSubscribe(!isSubscribe)
    } else{
      dispatch(userActions.unFollowUserAsync(userId))

      setisSubscribe(!isSubscribe)
    }
  }

  return (
    <div className="user__list-item list">
      <div className="list__data">
        <Link to={`/user/${user._id}`} className="user__img-wrapper">
          <img className="user__img" src={user.photo} alt=""/>
        </Link>
        <Link to={`/user/${user._id}`} className="user__name">{user.name}</Link>
      </div>
      {canSub ? <button data-testid={'subscribe-btn'} className="list__action" onClick={()=> handleSubscribe(user._id)}>{ isSubscribe ? "Unsubscribe": "Subscribe"}</button> : null }
    </div>
  );
};

UserItem.propTypes = {
  users: PropTypes.object,
  canSub: PropTypes.bool,
};

export default UserItem;