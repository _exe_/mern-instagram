import React, {useRef, useState} from "react";
import Comment from "../userComponents/Comment/Comment";
import "./PostDetails.scss";
import {commentPostFetch, likePost} from "../../utils/FetchToServer";
import {useSelector} from "react-redux";

const PostDetails = ({currentPost, closeModal, updateInfo}) => {
    const {
        _id: postId,
        title,
        photo,
        postedBy: {name: postAuthor, _id: userId, photo: photoAuthor},
        createdAt,
        updatedAt,
        comments,
        likes,
        body
    } = currentPost;

    const currentUser = useSelector(state => state.user);
    const index = likes.find((el) => el.id === currentUser._id);
    const [like, setLike] = useState(!!index);
    const [allLikeArr, setAllLikeArr] = useState(likes);

    const [valueComment, setValueComment] = useState("");
    const currentUserName = useSelector(state => state.user.name);
    const [commentUser, setCommentUser] = useState(comments);
    const commentInput = useRef();

    const commentsItems = comments.map(comment => (
        <div key={comment._id}>
            <Comment comments={comment}/>
        </div>
    ));

    const handleClickOnLike = async () => {
        await likePost(like, {userId, postId});
        setLike(!like);

        if (!like) {
            setAllLikeArr([...allLikeArr, currentUser]);
        } else {
            setAllLikeArr(allLikeArr.filter((el) => el.id !== currentUser.id));
        }
        await updateInfo(false);
    };

    const handleClickToAddComments = async () => {
        const comment = commentInput.current.value;
        await commentPostFetch({
            text: comment,
            postId: postId
        });
        setCommentUser([...commentUser, {user: currentUserName, text: comment}]);
        await updateInfo(false);
    };

    return (
        <div className="background" onClick={closeModal}>
            <div className="post_detail__container" onClick={preventCloseModal}>
                <div className="post_detail__image_container">
                    <img src={photo} alt="User avatar" className="post_detail__image"/>
                    <div className="post_detail__likes" onClick={() => handleClickOnLike(userId, postId)}>
                        <i className="fas fa-heart"/>{likes.length}
                    </div>
                </div>
                <div className="post_detail__info">
                    <div className="post_detail__author">
                        <div className="post_detail__author_avatar">
                            <img src={photoAuthor} alt="" className="avatar"/>
                        </div>
                        <p className="post_detail__author_name">{postAuthor}</p>
                    </div>
                    <div className="post_detail__header">
                        <div className="post_detail__detail">
                            <p className="post_detail__title">{title}</p>
                            <div>
                                <span className="post_detail__date">{createdAt}</span>
                                <span className="post_detail__date">{updatedAt}</span>
                            </div>
                            <p>{body}</p>

                        </div>


                    </div>
                    <div className="post_detail__comments_block">{comments.length !== 0 && commentsItems}</div>
                    <div className="add_comment">
                        <textarea placeholder="Add a comment" cols="30" rows="3" className="add_comment__input"
                                  onChange={(e) => setValueComment(e.target.value)}
                                  value={valueComment} ref={commentInput}/>
                        <input type="submit" value="Add comment" onClick={handleClickToAddComments}/>
                    </div>
                </div>

            </div>
        </div>
    );
};

const preventCloseModal = (e) => {
    e.stopPropagation();
};

export default PostDetails;