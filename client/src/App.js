import React from 'react';
import AppRouters from "./router/AppRouters";
import './App.scss'

function App() {
  return (
   <AppRouters/>
  );
}

export default App;
